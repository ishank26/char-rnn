#!/bin/env python3

from datetime import datetime
from keras.models import Sequential
from keras.layers.core import TimeDistributedDense, Dropout, Activation
from keras.layers.recurrent import LSTM
import numpy as np
import sys

startTime = datetime.now()
with open("input.txt", "r") as fin:
	text = fin.read()

charset = sorted(set(text))  # sorted list of all characters in input
num_char = int(len(charset)) # total number of characters in charset

# map characters to integers
char_to_index = dict((c, i) for i, c in enumerate(charset))
index_to_char = dict((i, c) for i, c in enumerate(charset))

line_length = 200  # seq size

# map input characters to index
input_to_index = [char_to_index[c] for c in text]

# convert char_index to seq of len = 200 and remove extra characters
x = input_to_index[:len(input_to_index) - len(input_to_index) % line_length]

# convert char_index to matrix of dim -> ( seq no. , seq size)
x = np.array(x, dtype="int32").reshape((-1, line_length))

# y is zeo matrix of 3d shape, dim -> (seq no. , seq size, len(charset))
y = np.zeros((x.shape[0], x.shape[1], num_char), dtype="int32")

# convert zero matrix to identity matrix with 1 at char_index pos in z axis and else 0
for i in range(x.shape[0]):
    for j in range(x.shape[1]):
        y[i, j, x[i, j]] = 1

# seq rolling will account for space before the word seq i.e
# seq = b-a-t-\n-m-a-n-\n
# roll_seq = \n-b-a-t-\n-m-a-n
y_roll = np.roll(y, 1, axis=1)  # shift right 1 columns
y_roll[:, 0, :] = 0   # change column 1 to 0
y_roll[:, 0, char_to_index["\n"]] = 1  # chage "\n" char_index to 1

###model###
hidden_neurons = 512
model = Sequential()

# layer 1
model.add(LSTM(hidden_neurons, return_sequences=True, input_shape=(None , num_char)))
model.add(Dropout(0.2))

# layer 2
model.add(LSTM(hidden_neurons, return_sequences=True))
model.add(Dropout(0.2))

# layer 3
model.add(LSTM(hidden_neurons, return_sequences=True))
model.add(Dropout(0.2))

# connect layers
print ("Connecting layers")
model.add(TimeDistributedDense(num_char))
model.add(Activation("softmax"))
print ("Connected")
# compile and fit
print ("Compiling model")
model.compile(loss="categorical_crossentropy", optimizer="rmsprop")
print("Complied")
print ("Fitting model")
model.fit(y_roll, y, batch_size=100, nb_epoch=20, show_accuracy=True)
print ("Model trained")
#model.compile(optimizer="rmsprop", loss='categorical_crossentropy')
#print ("Model compiled")

# map predict character to history vector
def encode_text(src, dst):
    for i, c in enumerate(src):
        dst[i, char_to_index[c]] = 1

def sample(a, temperature=1.0):
    a = np.log(a)/temperature
    a = np.exp(a)/np.sum(np.exp(a))
    return np.argmax(np.random.multinomial(1,a,1))

def generate_text(model,count=line_length, seed="\n"):
    out = seed
    # history vector to maintain how far lstm will see.
    # Maintans a vector of last 200 char seq
    history = np.zeros((1, count, num_char), dtype="int32")
    encode_text(out, history[0])
    while len(out) < count:
        prob_dist = model.predict(history[:, :len(out), :], verbose=0)[0][len(out)-1]
        # char = index_to_char[np.random.choice(charset, p=prob_dist)]
        char = index_to_char[sample(prob_dist,0.5)] # 0.5 = diversity , can be changed to improve accuracy ?
        #print (char)
        out+=char
        encode_text(out,history[0])
    return out

print (generate_text(model))
print ("Execution time\t{}".format(datetime.now() - startTime))
